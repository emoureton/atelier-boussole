const Prismic = require("@prismicio/client");

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    __dangerouslyDisableSanitizers: ["script"],
    title: "L'atelier & La boussole",
    htmlAttrs: {
      lang: "fr",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {
      src: "~/assets/styles/global.scss",
      lang: "scss",
    },
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/stripe.js", ssr: false },
    { src: "~/plugins/datepicker.js", ssr: false },
    { src: "~/plugins/skeleton.js", ssr: false },
    { src: "~/plugins/services.js" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: "~/components", extensions: ["vue"] }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/prismic",
    "@nuxt/image",
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Poppins: {
            wght: [300, 400, 600, 700],
          },
        },
        display: "swap",
        prefetch: true,
        preconnect: true,
        preload: true,
        download: true,
        base64: false,
        stylePath: "styles/_fonts.scss",
      },
    ],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    [
      "@nuxtjs/prismic",
      {
        endpoint: "https://atelierboussole.prismic.io/api/v2" || "",
      },
    ],
    "nuxt-sm",
    "@nuxtjs/style-resources",
    "@nuxtjs/svg",
    "@nuxt/image",
    "@nuxtjs/sitemap",
    "vue-plausible",
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: "/",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ["@prismicio/vue", "vue-slicezone"],
  },

  prismic: {
    endpoint: "https://atelierboussole.prismic.io/api/v2",
    modern: true,
    apiOptions: {
      routes: [
        {
          type: "article",
          path: "/actualité/:uid",
        },
      ],
    },
  },

  styleResources: {
    scss: [
      "~/assets/styles/_breakpoints.scss",
      "~/assets/styles/_animations.scss",
      "~/assets/styles/_variables.scss",
    ],
  },

  env: {
    STRIPE_PUBLISHABLE_KEY: process.env.STRIPE_PUBLISHABLE_KEY,
    STRIPE_SECRET_KEY: process.env.STRIPE_SECRET_KEY,
    SMTP_HOST: process.env.SMTP_HOST,
    SMTP_PORT: process.env.SMTP_PORT,
    SMTP_USER: process.env.SMTP_USER,
    SMTP_PASS: process.env.SMTP_PASS,
    SMTP_MAIL: process.env.SMTP_MAIL,
    GOOGLE_CALENDAR_ID: process.env.GOOGLE_CALENDAR_ID,
    FIRESTORE_PROJECT_ID: process.env.IRESTORE_PROJECT_ID,
    APP_URL: process.env.APP_URL,
    MAIL_ADMIN: process.env.MAIL_ADMIN,
    SERVICE_ACCOUNT_MAIL: process.env.SERVICE_ACCOUNT_MAIL,
    SERVICE_ACCOUNT_KEY: process.env.SERVICE_ACCOUNT_KEY,
    AXEPTIO_ID: process.env.AXEPTIO_ID,
  },

  serverMiddleware: [{ path: "/api", handler: "~/api" }],

  sitemap: {
    exclude: [
      // Backend part
      "/api/**",
      // Prismic reserved part
      "/slice-simulator",
      "/preview",
      // Useless part
      "/reservation/success",
    ],
    routes: async () => {
      const api = await Prismic.getApi(
        "https://atelierboussole.prismic.io/api/v2"
      );
      const posts = await api.query(
        Prismic.Predicates.at("document.type", "article")
      );
      const postsRoutes = posts.results.map((v) => "/actualités/" + v.uid);

      const events = await api.query(
        Prismic.Predicates.at("document.type", "events")
      );
      const eventsRoutes = events.results.map((v) => "/events/" + v.uid);

      const routes = eventsRoutes.concat(postsRoutes);
      return routes;
    },
  },

  image: {
    provider: "static",
  },

  plausible: {
    domain: "atelierboussole.fr",
  },
};
