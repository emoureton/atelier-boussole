# API routes

### /api/calendar :

/list :

- returns : list of Google Calendar Events
- format : JSON

/create :

- accept : JSON of shape :
    
    ```jsx
    event: {
        summary: "Event title",
        description: "Even description",
        start: {
            dateTime: '2022-04-08T01:00:00+02:00',
            timeZone: "Europe/Paris",
        },
        end: {
            dateTime: '2022-04-08T02:00:00+02:00',
            timeZone: "Europe/Paris",
        },
        reminders: {
            useDefault: false,
            overrides: [
            { method: "email", minutes: 24 * 60 },
            { method: "popup", minutes: 30 },
            ],
        },
      },
    ```
    

---

### /api/mail :

/send :

- accept : JSON of shape :
    
    ```jsx
    mail: "Client email"
    event: {
    	// Like in /api/calendar/create
    }
    ```
    

/message : 

- accept : JSON of shape :
    
    ```jsx
    data = {
      lastName: this.lastName,
      firstName: this.firstName,
      email: this.email,
      phone: this.phone,
      message: this.message,
    }
    ```