import { AtelierBoussoleApi } from "../service/api";
import { EventsApi } from "../service/events";
import { CheckoutApi } from "../service/checkout"; 

export default ({ app }, inject) => {
  const api = new AtelierBoussoleApi();
  inject("atelierboussole", api);

  const events = new EventsApi();
  inject("events", events);

  const checkout = new CheckoutApi();
  inject("checkout", checkout);
};
