import axios from 'axios'

export class CheckoutApi {
  constructor() {}

  getCheckout({ price, order, name, metadata, successUrl, cancelUrl }) {
    try {
      const req = axios.post("/api/payment/checkout", {
        order: order,
        price: price,
        name: name,
        metadata: metadata,
        successUrl: successUrl,
        cancelUrl: cancelUrl,
      });
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  }

  getSession({ id }) {
    try {
      const req = axios.get("/api/payment/" + id);
      const data = req.then((r) => r.data);
      return data
    } catch(e) {
      return e;
    }
  }
}