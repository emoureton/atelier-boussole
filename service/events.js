import { addZeroToTime } from '../helpers/functions'
import axios from "axios"

export class EventsApi {
  constructor() {}

  getCalendar = () => {
    try {
      const req = axios.get("/api/calendar/list");
      const data = req.then(({ data }) => data.result);
      return data;
    } catch (e) {
      return e;
    }
  };

  createEvent = ({ event }) => {
    try {
      const req = axios.post("/api/calendar/create", event);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  checkAvailability = (events, start, end) => {
    start =
      addZeroToTime(start.toFixed()) + ":" + (start % 1 === 0 ? "00" : "30");
    end = addZeroToTime(end.toFixed()) + ":" + (end % 1 === 0 ? "00" : "30");

    // Handle empty journey
    if (events.length === 0) {
      return true;
    }

    for (let i = 0; i < events.length; i++) {
      const el = events[i]
      const elTimes = {
        start:
          addZeroToTime(new Date(el.start.dateTime).getHours()) +
          ":" +
          addZeroToTime(new Date(el.start.dateTime).getMinutes()),
        end:
          addZeroToTime(new Date(el.end.dateTime).getHours()) +
          ":" +
          addZeroToTime(new Date(el.end.dateTime).getMinutes()),
      };

      if (!(elTimes.end <= start) && !(end <= elTimes.start)) {
        return false;
      }
    }

    return true;
  };

  getEvent({ id }) {
    try {
      const req = axios.get("/api/events/" + id);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  }

  bookEvent({ id, payload }) {
    try {
      const req = axios.post("/api/events/book/" + id, payload);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  }
}
