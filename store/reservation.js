import { replaceAtString } from '../helpers/functions'

const initalStartDate = replaceAtString(
  "2022-04-08T08:00:00+02:00",
  0,
  new Date().toISOString().slice(0, 10)
);
const initalEndDate = replaceAtString(
  "2022-04-08T09:00:00+02:00",
  0,
  new Date().toISOString().slice(0, 10)
);

export const state = () => ({
  requestedDay: initalStartDate.slice(0, 10),
  schedule: [],
  clientEmail: "john@doe.com",
  requestedDuration: 0.5,
  requestedStart: 10,
  requestedEnd: 11,
  requestedType: "free",
  event: {
    summary: "Atelier & la Boussole - X - John Doe",
    description: "Rendez-vous",
    start: {
      dateTime: initalStartDate,
      timeZone: "Europe/Paris",
    },
    end: {
      dateTime: initalEndDate,
      timeZone: "Europe/Paris",
    },
    reminders: {
      useDefault: false,
      overrides: [
        { method: "email", minutes: 24 * 60 },
        { method: "popup", minutes: 30 },
      ],
    },
  },
  code: {
    id: undefined,
    codeData: undefined,
  },
});

export const mutations = {
  changeDay(state, date) {
    state.requestedDay = date.slice(0, 10);
  },

  changeSchedule(state, { schedule }) {
    state.schedule = schedule;
  },

  changeEmail(state, { email }) {
    state.clientEmail = email;
  },

  changeDuration(state, { duration }) {
    state.requestedDuration = duration;
  },

  changeType(state, { type }) {
    state.requestedType = type;
  },

  updateSummary(state, summary) {
    state.event.summary = summary;
  },

  updateDate(state, date) {
    state.event.start.dateTime = replaceAtString(
      state.event.start.dateTime,
      0,
      date.slice(0, 10)
    );
    state.event.end.dateTime = replaceAtString(
      state.event.end.dateTime,
      0,
      date.slice(0, 10)
    );
  },

  updateStart(state, start) {
    state.requestedStart = start[0];
    state.requestedEnd = start[1];

    let startTime = 0;
    let endTime = 0;

    if (Number(start[0]) === start[0] && start[0] % 1 === 0) {
      startTime =
        (start[0].toString().length !== 1 ? "" : "0") +
        start[0].toString() +
        ":00";
    } else {
      startTime =
        (parseInt(start[0]).toString().length !== 1 ? "" : "0") +
        parseInt(start[0]).toString() +
        ":30";
    }

    if (Number(start[1]) === start[1] && start[1] % 1 === 0) {
      endTime =
        (start[1].toString().length !== 1 ? "" : "0") +
        start[1].toString() +
        ":00";
    } else {
      endTime =
        (parseInt(start[1]).toString().length !== 1 ? "" : "0") +
        parseInt(start[1]).toString() +
        ":30";
    }

    state.event.start.dateTime = replaceAtString(
      state.event.start.dateTime,
      11,
      startTime
    );
    state.event.end.dateTime = replaceAtString(
      state.event.end.dateTime,
      11,
      endTime
    );
  },

  changeCode(state, data) {
    state.code.id = data;
  },

  changeCodeData(state, data) {
    state.code.codeData = data;
  },
};