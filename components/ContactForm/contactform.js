export default {
  data() {
    return {
      lastName: "",
      firstName: "",
      email: "",
      phone: "",
      message: "",
      error: undefined,
      inputs: undefined,
      state: undefined
    };
  },
  mounted() {
    this.inputs = document.querySelectorAll(
      ".contact-form input, .contact-form textarea"
    );
  },
  methods: {
    async handleSubmit() {
      const data = {
        lastName: this.lastName,
        firstName: this.firstName,
        email: this.email,
        phone: this.phone,
        message: this.message,
      };

      this.inputs.forEach((el) => {
        if(el.value === '') {
          el.classList.add("inputerror");
        }
      })

      for(let field in data) {
        if(data[field] == '') {
          return; 
        }
      }

      this.state = 'Envoi du message'
      const send = await this.$atelierboussole.sendMessage({ payload: data })
      if(send === 'success') {
        this.state = 'Message envoyé !'
      } else {
        this.state = 'Il y a eu une erreur...'
      }
    },

    inputChange(e) {
      this.error = ""
      if (e.target.value !== "") {
        e.target.classList.remove("inputerror");
      }
    }
  },
};
