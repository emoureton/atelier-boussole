import Facebook from "../../../assets/img/facebook.svg?inline";
import Instagram from "../../../assets/img/instagram.svg?inline";
import Linkedin from "../../../assets/img/linkedin.svg?inline";
export default {
  components: {
    Facebook,
    Instagram,
    Linkedin,
  },
};
