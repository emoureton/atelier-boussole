import Compass from "~/assets/img/compass.svg?inline";

export default {
  components: { Compass },

  data() {
    return {
      hover: false,
    };
  },
};
