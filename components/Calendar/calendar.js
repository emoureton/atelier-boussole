export default {
  data() {
    return {
      events: {},
      schedules: [],
      choice: [],
    };
  },
  async mounted() {
    this.events = await this.$events.getCalendar()
    this.getSchedules();
  },
  watch: {
    requestedDuration() {
      this.getSchedules();
    },

    requestedDay() {
      this.getSchedules();
      this.choice = []
    },

    choice() {
      if(this.choice.length !== 0) {
        this.$nuxt.$emit('schedule-change', true)
      } else {
        this.$nuxt.$emit("schedule-change", false);
      }
    }
  },
  computed: {
    event() {
      return this.$store.state.reservation.event;
    },
    requestedDuration() {
      return this.$store.state.reservation.requestedDuration;
    },
    requestedDay() {
      return this.$store.state.reservation.requestedDay;
    },
  },
  methods: {
    async getSchedules() {
      const duration = parseFloat(this.requestedDuration);
      let start = 8;
      let end = 18;
      const schedules = [];
      const filteredEvents = this.filterEvents()
      for (let i = start; i <= end - duration; i += duration) {
        const result = await this.$events.checkAvailability(
          filteredEvents,
          i,
          i + duration
        );
        const item = [i, i + duration, result];
        schedules.push(item);
      }
      this.schedules = schedules;
    },

    filterEvents() {
      const events = this.events.filter(
        (el) =>
          (new Date(el.start.dateTime).getDate() ===
            new Date(this.requestedDay).getDate() &&
            new Date(el.start.dateTime).getMonth() ===
              new Date(this.requestedDay).getMonth() &&
            new Date(el.start.dateTime).getFullYear() ===
              new Date(this.requestedDay).getFullYear()) ||
          (new Date(el.end.dateTime).getDate() ===
            new Date(this.requestedDay).getDate() &&
            new Date(el.end.dateTime).getMonth() ===
              new Date(this.requestedDay).getMonth() &&
            new Date(el.end.dateTime).getFullYear() ===
              new Date(this.requestedDay).getFullYear())
      );
      return events
    },

    handleChoice(e) {
      const choice = this.schedules[e.target.getAttribute("index")];
      this.choice = choice;
      this.$store.commit("reservation/updateStart", choice);
    },
  },
};
