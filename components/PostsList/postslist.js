import { components } from "~/slices";

export default {
  data() {
    return { components };
  },
  props: {
    posts: Array,
    columns: String,
    avoid: String,
  },
  mounted() {
    if(this.avoid) {
      this.posts.results = this.posts.results.filter(res => res.id !== this.avoid)
    }
  }
};
