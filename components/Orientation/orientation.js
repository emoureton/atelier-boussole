import CheckIcon from '../../assets/img/check.svg?inline'
import CompassIcon from "../../assets/img/compass.svg?inline";
import CloseIcon from "../../assets/img/close.svg?inline";

const data = {
  q1: ["Dans ma vie perso'", "Dans ma vie pro'", "Non"],
  q2: {
    0: [
      "Espace de paroles",
      "Rencontrer d'autres parents",
      "Equilibrer ma vie pro et perso",
      "Trouver un pro' spécifique",
    ],
    1: [
      "Rencontrer vos pairs",
      "Monter en compétences",
      "Trouver du soutien pour le passage du CAP AEPE",
    ],
    2: [
      "Réaliser une reconversion",
      "Monter en compétences",
      "Trouver du soutien sur parcoursup",
      "Espace de paroles",
    ],
  },
};

const resultsData = [
  // Résultats étape 1
  [
    // Résultats étape 2
    [
      {
        title: "Ateliers parentalité",
        link: "/reservation",
      },
      {
        title: "Accompagnements",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Café rencontre",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Ateliers",
        link: "/reservation",
      },
      {
        title: "Bilan de compétences",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Ateliers",
        link: "/reservation",
      },
      {
        title: "Accompagnements",
        link: "/reservation",
      },
    ],
  ],

  [
    [
      {
        title: "Café rencontre",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Ateliers",
        link: "/reservation",
      },
      {
        title: "Accompagnements",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Formations CAP AEPE",
        link: "/reservation",
      },
      {
        title: "Accompagnement",
        link: "/reservation",
      },
    ],
  ],

  [
    [
      {
        title: "Bilan de compétences",
        link: "/reservation",
      },
      {
        title: "Accompagnement emploi",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Ateliers",
        link: "/reservation",
      },
      {
        title: "Accompagnement",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Accompagnement Parcoursup",
        link: "/reservation",
      },
    ],
    [
      {
        title: "Ateliers",
        link: "/reservation",
      },
      {
        title: "Accompagnement",
        link: "/reservation",
      },
    ],
  ],
];

export default {
  components: {
    CheckIcon,
    CompassIcon,
    CloseIcon,
  },
  data() {
    return {
      steps: ["q1", "q2", "r"],
      questions: {
        q1: {
          text: "Vous accompagnez des enfants ?",
          choices: data.q1,
        },

        q2: {
          text: "De quoi avez-vous besoin aujourd'hui ?",
          choices: data.q2[this.step],
        },

        r: {
          text: "Voici ce que l'on peut vous proposer :",
          choices: undefined,
        },
      },
      choices: {
        q1: undefined,
        q2: undefined,
      },
      step: 0,
      current: "q1",
    };
  },

  methods: {
    handleChoice(e) {
      this.choices[this.current] = e.target.getAttribute("value");
    },

    next() {
      this.step = this.step += 1;
    },

    handleClose() {
      this.$emit("close");
    },
  },

  watch: {
    choices: {
      handler() {
        if (this.step == 0) {
          const nextStep = this.steps[this.step + 1];
          this.questions[nextStep].choices =
            data[nextStep][this.choices[this.current]];
        } else {
          this.questions.r.choices =
            resultsData[parseInt(this.choices.q1)][parseInt(this.choices.q2)];
        }
      },
      deep: true,
      immediate: true,
    },

    step() {
      this.current = this.steps[this.step];
    },
  },

  mounted() {
    document.body.classList.add("no-scroll");
  },

  beforeDestroy() {
    document.body.classList.remove("no-scroll");
  },
};