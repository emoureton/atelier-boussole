import PasswordIcon from "css.gg/icons/svg/password.svg?inline";
export default {
  components: {
    PasswordIcon,
  },
  data() {
    this.pk = process.env.STRIPE_PUBLISHABLE_KEY;
    return {
      modal: false,
      codeData: undefined,
      code: undefined,
      ready: false,
    };
  },
  methods: {
    async checkout() {
      const id = await this.$atelierboussole.createOrder({
        payload: {
          event: this.event,
          mail: this.clientEmail,
          type: "payment",
          duration: this.requestedDuration,
          completed: false,
        },
      });
      const url = await this.$checkout.getCheckout({
        order: id,
        name: "conférence",
        metadata: {
          quantity: this.quantity,
          type: "meeting",
          mail: this.clientEmail,
        },
        successUrl: process.env.APP_URL + "/reservation/success/" + id,
        cancelUrl: process.env.APP_URL + "/reservation/" + id,
      });
      window.location.href = url;
    },

    async checkoutWithCode() {
      const id = await this.$atelierboussole.createOrder({
        payload: {
          event: this.event,
          mail: this.clientEmail,
          code: {
            id: this.codeStore,
            newCredit: parseInt(this.codeData.credit) - this.requestedDuration,
          },
          type: "code",
          duration: this.requestedDuration,
          completed: false,
        },
      });
      window.location.href = process.env.APP_URL + "/reservation/success/" + id;
    },

    async freeCheckout() {
      const id = await this.$atelierboussole.createOrder({
        payload: {
          event: this.event,
          mail: this.clientEmail,
          type: "free",
          duration: this.requestedDuration,
          completed: false,
        },
      });
      window.location.href = process.env.APP_URL + "/reservation/success/" + id;
    },

    async getCode() {
      const data = await this.$atelierboussole.getCode({ code: this.code });
      this.codeData = data;
      this.$store.commit("reservation/changeCodeData", data);
      this.modal = false;
    },
  },
  mounted() {
    this.quantity = this.requestedDuration * 2;

    this.$nuxt.$on("schedule-change", ($event) => {
      this.ready = $event === true ? true : false;
    });
  },
  computed: {
    clientEmail() {
      return this.$store.state.reservation.clientEmail;
    },

    requestedDuration() {
      return this.$store.state.reservation.requestedDuration;
    },

    requestedType() {
      return this.$store.state.reservation.requestedType;
    },

    clientSummary() {
      return this.$store.state.reservation.event.summary;
    },

    requestedDate() {
      return this.$store.state.reservation.event.start.dateTime;
    },

    event() {
      return this.$store.state.reservation.event;
    },

    codeStore() {
      return this.$store.state.reservation.code.id;
    },
  },

  watch: {
    requestedDuration() {
      this.quantity = this.requestedDuration * 2;
    },
    code() {
      this.$store.commit("reservation/changeCode", this.code);
    },
  },
};
