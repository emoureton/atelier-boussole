const express = require('express')
const app = express()
const cors = require("cors");
const bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(cors());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,OPTIONS,PATCH,DELETE,POST,PUT"
  );
  next();
});

const calendar = require('./calendar')
app.use(calendar)

const mail = require('./mail')
app.use(mail)

const reservation = require('./reservation')
app.use(reservation)

const payment = require("./payment");
app.use(payment);

if (require.main === module) {
  const port = 3001
  app.listen(port, () => {
    console.log(`API server listening on port ${port}`)
  })
}

module.exports = app