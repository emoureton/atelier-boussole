const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const ical = require("ical-generator")

let transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS,
  },
  tls: {
    rejectUnauthorized: false,
  },
});

router.post("/mail/send", async (req, res) => {
  const calendar = ical({ name: "Rendez-vous Atelier & La Boussole" });
  calendar.createEvent({
    start: req.body.event.start.dateTime,
    end: req.body.event.end.dateTime,
    summary: "Rendez-vous Atelier & La Boussole",
    location: "Google Meet",
    url: "https://atelier-boussole.vercel.app",
  });

  let mailOptions = {
    from: process.env.SMTP_MAIL,
    to: req.body.mail,
    subject: "Rendez-vous Atelier & La Boussole",
    text: "Votre rendez-vous est confirmé",
    html: `
    <div style="">
      <h1>Hello !</h1>
      <h2>Merci pour votre réservation</h2>
      <p>Votre réservation :</p>
      <p>${new Date(req.body.event.start.dateTime).toLocaleDateString("fr")}</p>
      <p>Horaire : ${req.body.event.start.dateTime.slice(
        11,
        16
      )}h à ${req.body.event.end.dateTime.slice(11, 16)}h</p>
    </div>
    `,
    icalEvent: {
      filename: "invitation.ics",
      method: "request",
      content: calendar.toString(),
    },
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.json(error);
    } else {
      res.json("Email sent: " + info.response);
    }
  });
});

router.post("/mail/message", async (req, res) => {
  let mailOptions = {
    from: process.env.SMTP_MAIL,
    to: process.env.MAIL_ADMIN,
    subject:
      "Nouvelle demande de contact de " +
      req.body.lastName +
      req.body.firstName,
    text: "De la part de " + req.body.lastName + req.body.firstName,
    html: `
    <div style="">
      <p>De la part de  : ${req.body.lastName} ${req.body.firstName}</p>
      <p>Téléphone : ${req.body.phone} </p>
      <p>Mail : ${req.body.email} </p>
      <p>Message : ${req.body.message}</p>
    </div>
    `,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.json(error);
    } else {
      res.json('success');
    }
  });
});

module.exports = router;
