const express = require('express')
const router = express.Router()

const { google } = require('googleapis');
const { JWT } = require('google-auth-library')

const SCOPES = [
  "https://www.googleapis.com/auth/calendar",
  "https://www.googleapis.com/auth/calendar.events",
];

const client = new JWT(
  process.env.SERVICE_ACCOUNT_MAIL,
  null,
  process.env.SERVICE_ACCOUNT_KEY,
  SCOPES,
  null
);

google.options({
  auth: client
})

router.get('/calendar/list', async (req, res) => {
  client.authorize((authErr) => {
    if (authErr) {
      return;
    }

    const calendar = google.calendar({ version: "v3" });

    calendar.events.list({
      calendarId: process.env.GOOGLE_CALENDAR_ID,
      timeMin: (new Date()).toISOString(),
      singleEvents: true,
      orderBy: 'startTime',
    }, (err, response) => {
      if (err) {
        console.log(err)
      }
      if(response) {
        res.json({ result: response.data.items });
      } else {
        res.json('echec')
      }
    });
  })
})

router.post('/calendar/create', (req, res) => {
  client.authorize((authErr) => {
    if (authErr) {
      console.log(authErr);
      return;
    }

    const calendar = google.calendar({ version: "v3" });

    calendar.events.insert({
      calendarId: process.env.GOOGLE_CALENDAR_ID,
      resource: req.body
    }, (err, response) => {
      if (err) {
        console.log(err)
      }
      res.json({ result: 'event created' })
    });
  })
})

module.exports = router