const express = require('express')
const router = express.Router()
const Prismic = require("@prismicio/client");

import { db, fs } from './utils/db'
 
router.get("/order/by/:id", async (req, res) => {
  let result = [];
  const order = await db.collection("orders").where('code.id', '==', req.params.id).get();
  order.forEach((el) => {
    const item = [];
    item.push(el.id);
    item.push(el.data());
    result.push(item);
  });
  res.json(result);
});

router.get('/order/:id', async (req, res) => {
  const order = await db.collection('orders').doc(req.params.id).get();
  if (!order.exists) {
    res.json('No document');
   } else {
    res.json(order.data());
   }
})

router.post('/order/create', async (req, res) => {
  const order = await db.collection('orders').add(req.body);
  res.json(order.id)
})

router.post('/order/set/:id', async (req, res) => {
  await db.collection('orders').doc(req.params.id).update(req.body);
  res.json('Updated')
})

router.post('/code/create', async (req, res) => {
  const code = await db.collection("codes").add(req.body);
  res.json(code.id);
})

router.post("/code/set/:id", async (req, res) => {
  await db
    .collection("codes")
    .doc(req.params.id)
    .update(req.body);
  res.json("Updated");
});

router.get("/code/list", async (req, res) => {
  let result = [];
  const codes = await db.collection("codes").get();
  codes.forEach((doc) => {
    const item = []
    item.push(doc.id);
    item.push(doc.data());
    result.push(item)
  });
  res.json(result);
});

router.get("/code/:id", async (req, res) => {
  const code = await db.collection("codes").doc(req.params.id).get();
  if (!code.exists) {
    res.json("No document");
  } else {
    res.json(code.data());
  }
});

router.post("/code/delete/:id", async (req, res) => {
  await db.collection("codes").doc(req.params.id).delete();
  res.json("Deleted");
});

router.get('/events/:id', async (req, res) => {
  const el = await db.collection("events").doc(req.params.id).get();
  res.json(el.data())
})

router.post("/events/create/", async (req, res) => {
  let id = req.body.id ? req.body.id : req.body.documents[0];
  const prismic = await Prismic.getApi(
    "https://atelierboussole.prismic.io/api/v2"
  );
  const doc = await prismic.getByID(id);
  const el = await db
    .collection("events")
    .doc(id)
    .set({
      price: doc.data.price,
    }, { merge: true });
  res.json(el);
});

router.post("/events/book/:id", async (req, res) => {
  const existing = await db
    .collection("events")
    .where("mail", "==", req.body.mail)
    .get();
  if(existing.length > 0) {
    res.json('déja inscrit')
    return
  }
    await db
    .collection("events")
    .doc(req.params.id)
    .update({
      persons: fs.firestore.FieldValue.arrayUnion(req.body),
    });
  res.json('success');
});

module.exports = router