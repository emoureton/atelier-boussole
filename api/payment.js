const express = require("express");
const router = express.Router();
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
import { db } from "./utils/db";

router.post("/payment/checkout", async (req, res) => {
  let price;
  if(req.body.metadata.type === "event") {
    const event = await db
      .collection("events")
      .doc(req.body.metadata.event)
      .get();
    price = event.data().price;
  } else {
    price = req.body.metadata.quantity * 75;
  }

  const session = await stripe.checkout.sessions.create({
    customer_email: req.body.metadata.mail,
    line_items: [
      {
        price_data: {
          currency: "eur",
          product_data: {
            name: req.body.name,
          },
          unit_amount: price * 100,
        },
        quantity: 1,
      },
    ],
    metadata: req.body.metadata,
    mode: "payment",
    success_url: req.body.successUrl + "?session_id={CHECKOUT_SESSION_ID}",
    cancel_url: req.body.cancelUrl,
  });
  res.json(session.url)
});

router.get("/payment/:id", async (req, res) => {
  const session = await stripe.checkout.sessions.retrieve(req.params.id);
  res.json(session)
})

module.exports = router;
