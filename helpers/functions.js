export const replaceAtString = (string, index, replacement) => {
  return string.substring(0, index) + replacement + string.substring(index + replacement.length);
}

export const addZeroToTime = (number) => {
  const newNumber =
    (number.toString().length === 1 ? "0" : "") + number.toString();
  return newNumber;
};